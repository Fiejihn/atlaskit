# Checkbox

Checkboxes are used in and out of forms to indicate a switch between two binary states.

## Try it out

Detailed docs and example usage can be found [here](https://aui-cdn.atlassian.com/atlaskit/stories/@NAME@/@VERSION@/).

## Installation

```sh
npm install @NAME@
```
