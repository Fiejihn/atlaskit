import styled from 'styled-components';

const RequestTrialFooter = styled.div`
  text-align: right;
`;

RequestTrialFooter.displayName = 'RequestTrialFooter';
export default RequestTrialFooter;
