import styled from 'styled-components';

import { gridSize, math } from '@atlaskit/theme';

const GrantAccessChangeUsersDiv = styled.div`
  margin-top: -${math.multiply(gridSize, 3.625)}px;
`;

GrantAccessChangeUsersDiv.displayName = 'GrantAccessChangeUsersDiv';
export default GrantAccessChangeUsersDiv;
