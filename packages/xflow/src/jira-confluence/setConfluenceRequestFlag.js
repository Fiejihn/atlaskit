import { setAlreadyRequestedFlag } from '../common/alreadyRequestedFlag';

export default () => setAlreadyRequestedFlag('confluence.ondemand');
