import grantAccessToUsers from '../common/grantAccessToUsers';

export default grantAccessToUsers('confluence-users', 'confluence');
