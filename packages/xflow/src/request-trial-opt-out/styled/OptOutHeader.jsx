import styled from 'styled-components';

import { gridSize } from '@atlaskit/theme';

const OptOutHeader = styled.h3`
  padding: ${gridSize}px 0px 0px ${gridSize}px;
`;

OptOutHeader.displayName = 'OptOutHeader';
export default OptOutHeader;
