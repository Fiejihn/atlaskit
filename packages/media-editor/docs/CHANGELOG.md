# @atlaskit/media-editor

## 3.2.0 (2017-08-11)

* feature; bump :allthethings: ([f4b1375](https://bitbucket.org/atlassian/atlaskit/commits/f4b1375))





## 3.1.1 (2017-07-25)


* fix; use class transform in loose mode in babel to improve load performance in apps ([fde719a](https://bitbucket.org/atlassian/atlaskit/commits/fde719a))

## 3.1.0 (2017-06-15)


* feature; add ShapeDeleter component ([a5b8b01](https://bitbucket.org/atlassian/atlaskit/commits/a5b8b01))

## 3.0.0 (2017-05-22)


null bump media-core version to 8.0.0 ([a911076](https://bitbucket.org/atlassian/atlaskit/commits/a911076))


* breaking; Bump media-core version to 8.0.0

## 2.1.0 (2017-05-16)


* fix; pass alpha channel to the background ([0090d27](https://bitbucket.org/atlassian/atlaskit/commits/0090d27))


* feature; add storybook with transparent background ([28648df](https://bitbucket.org/atlassian/atlaskit/commits/28648df))

## 2.0.1 (2017-05-03)


* fix; copy the editor core to the dist ([57a9a9d](https://bitbucket.org/atlassian/atlaskit/commits/57a9a9d))

## 1.0.0 (2017-04-27)


* fix; export types required by exported functions ([145da2b](https://bitbucket.org/atlassian/atlaskit/commits/145da2b))

## 1.0.0 (2017-04-27)

## 1.0.0 (2017-04-27)

## 1.0.0 (2017-04-27)


* feature; add media-editor package ([5b4ae05](https://bitbucket.org/atlassian/atlaskit/commits/5b4ae05))


* breaking; Added media-editor package
