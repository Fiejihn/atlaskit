const path = require('path');

module.exports = [
  { name: 'ProgressDots', src: path.join(__dirname, '../src/components/Dots.js') },
];
