const path = require('path');

module.exports = [
  { name: 'AtlasKitThemeProvider', src: path.join(__dirname, '../src/AtlasKitThemeProvider.js') },
];
